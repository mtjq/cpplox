# CPPLOX

## Description

This is an implementation of the first half of the book 
[Crafting Interpreters](http://craftinginterpreters.com/) in C++. It consists 
in writing an interpreter for a scripting language created by the author of the 
book, LOX. The original implementation is in Java, so even though the languages 
can sometimes be quite similar, some design choices had to be made.

This is still a work in progress as only the fundations have been written so 
far.

## Design choices

I used `std::optional` to carry the eventual absence of literal, plus the 
errors that some functions may want to return; and `std::variant`/`std::visit` 
to implement the visitor pattern.

#include "scanner.h"
#include "runner.h"

std::unordered_map<std::string, TokenType> Scanner::keywords_{
  {"and", AND},
  {"class", CLASS},
  {"else", ELSE},
  {"false", FALSE},
  {"for", FOR},
  {"fun", FUN},
  {"if", IF},
  {"nil", NIL},
  {"or", OR},
  {"print", PRINT},
  {"return", RETURN},
  {"super", SUPER},
  {"this", THIS},
  {"true", TRUE},
  {"var", VAR},
  {"while", WHILE},
};

Scanner::Scanner(std::string_view source)
    : source_{source} {}

std::vector<Token> Scanner::ScanTokens() {
  while (!IsAtEnd()) {
    start_ = current_;
    ScanToken();
  }

  tokens_.push_back(Token(END_OF_FILE, "", line_));
  return tokens_;
}

void Scanner::ScanToken() {
  char c{Advance()};
  switch (c) {
    case '(':
      AddToken(LEFT_PAREN);
      break;
    case ')':
      AddToken(RIGHT_PAREN);
      break;
    case '{':
      AddToken(LEFT_BRACE);
      break;
    case '}':
      AddToken(RIGHT_BRACE);
      break;
    case ',':
      AddToken(COMMA);
      break;
    case '.':
      AddToken(DOT);
      break;
    case '-':
      AddToken(MINUS);
      break;
    case '+':
      AddToken(PLUS);
      break;
    case ';':
      AddToken(SEMICOLON);
      break;
    case '*':
      AddToken(STAR);
      break;

    case '!':
      AddToken(Match('=') ? BANG_EQUAL : BANG);
      break;
    case '=':
      AddToken(Match('=') ? EQUAL_EQUAL : EQUAL);
      break;
    case '<':
      AddToken(Match('=') ? LESS_EQUAL : LESS);
      break;
    case '>':
      AddToken(Match('=') ? GREATER_EQUAL : GREATER);
      break;

    case '/':
      if (Match('/')) {
        while (Peek() != '\n' && !IsAtEnd())
          Advance();
      } else {
        AddToken(SLASH);
      }

    case ' ':
    case '\r':
    case '\t':
      break;
    case '\n':
      line_++;
      break;

    case '"':
      String();
      break;

    default:
      if (IsDigit(c)) {
        Number();
      } else if (IsAlpha(c)){
        Identifier();
      } else {
        Runner::Error(line_, "Unexpected character.");
      }
      break;
  }
}

void Scanner::AddToken(TokenType type) {
  std::string_view text = source_.substr(start_, current_ - start_);
  tokens_.push_back(Token(type, text, line_));
}

template<class T>
void Scanner::AddToken(TokenType type, T literal) {
  static_assert(std::is_same_v<T, std::string> || std::is_same_v<T, double>);
  std::string_view text = source_.substr(start_, current_ - start_);
  tokens_.push_back(Token(type, text, literal, line_));
}

bool Scanner::Match(char expected) {
  if (IsAtEnd())
    return false;
  if (source_.at(current_) != expected)
    return false;

  current_++;
  return true;
}

char Scanner::Peek() {
  if (IsAtEnd())
    return '\0';
  return source_.at(current_);
}

char Scanner::PeekNext() {
  if (current_ + 1 >= source_.length())
    return '\0';
  return source_.at(current_ + 1);
}

char Scanner::Advance() {
  return source_.at(current_++);
}

void Scanner::String() {
  while (Peek() != '"' && !IsAtEnd()) {
    if (Peek() == '\n')
      line_++;
    Advance();
  }

  if (IsAtEnd()) {
    Runner::Error(line_, "Unterminated string.");
    return;
  }

  Advance();

  std::string value{
    static_cast<std::string>(source_.substr(start_ + 1, current_ - start_ - 2))
  };
  AddToken(STRING, value);
}

void Scanner::Number() {
  while (isdigit(Peek()))
    Advance();

  if (Peek() == '.' && isdigit(PeekNext())) {
    Advance();

    while (isdigit(Peek()))
      Advance();
  }

  AddToken(
      NUMBER,
      std::stod(static_cast<std::string>(source_.substr(start_, current_))));
}

void Scanner::Identifier() {
  while (IsAlphaNum(Peek())) {
    Advance();
  }

  std::string text{static_cast<std::string>(source_.substr(start_, current_))};
  auto _type{keywords_.find(text)};
  TokenType type;

  if (_type == keywords_.end()) {
      type = IDENTIFIER;
  } else {
    type = _type->second;
  }

  AddToken(type);
}

bool Scanner::IsAlpha(char c) {
  return ('a' <= c && c <= 'z') ||
         ('A' <= c && c <= 'Z') ||
         c == '_';
}

bool Scanner::IsDigit(char c) {
  return '0' <= c && c <= '9';
}

bool Scanner::IsAlphaNum(char c) {
  return IsAlpha(c) || IsDigit(c);
}

bool Scanner::IsAtEnd() {
  return (current_ >= source_.length());
}

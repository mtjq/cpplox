#include "expression.h"

// Helpers to make pointers to expressions
BinaryExprPtr CreateBinaryExprPtr(
    ExpressionPtr left, Token oper, ExpressionPtr right) {
  return std::make_unique<BinaryExpr>(
      std::move(left), oper, std::move(right));
}

GroupingExprPtr CreateGroupingExprPtr(ExpressionPtr expression) {
  return std::make_unique<GroupingExpr>(std::move(expression));
}

LiteralExprPtr CreateLiteralExprPtr() {
  return std::make_unique<LiteralExpr>();
}

LiteralExprPtr CreateLiteralExprPtr(Literal lit) {
  return std::make_unique<LiteralExpr>(lit);
}

LiteralExprPtr CreateLiteralExprPtr(bool b) {
  return std::make_unique<LiteralExpr>(b);
}

UnaryExprPtr CreateUnaryExprPtr(Token oper, ExpressionPtr right) {
  return std::make_unique<UnaryExpr>(oper, std::move(right));
}

// Expression structs
BinaryExpr::BinaryExpr(ExpressionPtr left, Token oper, ExpressionPtr right)
    : left_{std::move(left)},
      operator_{oper},
      right_{std::move(right)} {}

GroupingExpr::GroupingExpr(ExpressionPtr expression)
    : expression_{std::move(expression)} {}

LiteralExpr::LiteralExpr()
    : value_{} {}

LiteralExpr::LiteralExpr(Literal lit)
    : value_{lit} {}

LiteralExpr::LiteralExpr(bool b)
    : value_{b} {}

UnaryExpr::UnaryExpr(Token oper, ExpressionPtr right)
    : operator_{oper},
      right_{std::move(right)} {}


// Pretty printer
std::string AstPrinter::operator() (const BinaryExprPtr& expr) {
  return Parenthesize(expr->operator_.lexeme(), expr->left_, expr->right_);
}

std::string AstPrinter::operator() (const GroupingExprPtr& expr) {
  return Parenthesize("group", expr->expression_);
}

std::string AstPrinter::operator() (const LiteralExprPtr& expr) {
  if (const Literal* lit = std::get_if<Literal>(&expr->value_)) {
    return LiteralToString(*lit);
  } else if (const bool* b = std::get_if<bool>(&expr->value_)) {
    return std::to_string(*b);
  }
  return "nil";
}

std::string AstPrinter::operator() (const UnaryExprPtr& expr) {
  return Parenthesize(expr->operator_.lexeme(), {expr->right_});
}

std::string Parenthesize(std::string_view name, ExpressionPtr& expr) {
  return "(" + std::string(name) + " " + std::visit(AstPrinter {}, expr) + ")";
}

std::string Parenthesize(
    std::string_view name, ExpressionPtr& expr1, ExpressionPtr& expr2) {
  return "(" + std::string(name) + " " + std::visit(AstPrinter {}, expr1)
    + " " + std::visit(AstPrinter {}, expr2) + ")";
}

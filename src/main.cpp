#include <iostream>
#include <string>
#include <format>

#include "runner.h"

int main(int argc, char* argv[]) {
  Runner runner{Runner()};
  if (argc > 2) {
    std::cout << "Usage: cpplox [script].\n";
  } else if (argc == 2) {
    std::cout << "Interpreting script " << argv[1] << ".\n";
    runner.RunFile(argv[1]);
  } else {
    std::cout << "REPL.\n";
    runner.RunPrompt();
  }
}

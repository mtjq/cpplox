#include <fstream>
#include <format>

#include "runner.h"
#include "scanner.h"
#include "parser.h"
#include "token.h"

bool Runner::has_error_{false};

Runner::Runner() {}

void Runner::Run(std::string_view source) {
  Scanner scanner{Scanner(source)};
  std::vector<Token> tokens = scanner.ScanTokens();

  Parser parser{tokens};
  OptionalExpressionPtr opt_expr = parser.Parse();

  if (has_error_ || !opt_expr.has_value())
    return;

  ExpressionPtr expr = std::move(opt_expr.value());

  std::cout << std::visit(AstPrinter {}, expr) << std::endl;

  // for (const Token& token: tokens) {
  //   std::cout << token.ToString() << "\n";
  // }
}

void Runner::RunFile(char* file_path) {
  std::ifstream script;
  script.open(file_path, std::ios::in);
  std::string line;
  while (getline(script, line)) {
    Run(line);
  }
  script.close();
}

void Runner::RunPrompt() {
  std::string line;
  while (true) {
    std::cout << "> ";
    if (std::getline(std::cin, line)) {
      Run(line);
    } else {
      break;
    }
  }
}

void Runner::Report(int line,
                    std::string_view where,
                    std::string_view message) {
  std::cerr << std::format("[line {}] Error{}: {}.\n", line, where, message);
  has_error_ = true;
};

void Runner::Error(int line, std::string_view message) {
  Report(line, "", message);
}

void Runner::Error(Token token, std::string_view message) {
  if (token.type() == END_OF_FILE) {
    Report(token.line(), " at end", message);
  } else {
    Report(token.line(), " at '" + std::string(token.lexeme()) + "'", message);
  }
}

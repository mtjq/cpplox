#pragma once

#include <string>
#include <format>
#include <variant>

enum TokenType {
  // Single-character tokens.
  LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE,
  COMMA, DOT, MINUS, PLUS, SEMICOLON, SLASH, STAR,

  // One or two character tokens.
  BANG, BANG_EQUAL, EQUAL, EQUAL_EQUAL,
  GREATER, GREATER_EQUAL, LESS, LESS_EQUAL,

  // Litterals.
  IDENTIFIER, STRING, NUMBER,

  // Keywords.
  AND, CLASS, ELSE, FALSE, FUN, FOR, IF, NIL, OR,
  PRINT, RETURN, SUPER, THIS, TRUE, VAR, WHILE,

  END_OF_FILE
};

using Literal = std::variant<double, std::string>;
using OptionalLiteral = std::optional<Literal>;

std::string LiteralToString(const Literal& literal);

std::string TokenTypeToString(TokenType token_type);

class Token {
 private:
  TokenType type_;
  std::string_view lexeme_;
  OptionalLiteral literal_;
  int line_;

 public:
  Token(TokenType type, std::string_view lexeme, int line);
  Token(TokenType type, std::string_view lexeme, double literal, int line);
  Token(TokenType type, std::string_view lexeme, std::string literal, int line);
  TokenType type() const;
  std::string_view lexeme() const;
  OptionalLiteral optional_literal() const;
  int line() const;
  std::string ToString() const;
};

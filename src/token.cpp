#include "token.h"

std::string LiteralToString(const Literal& literal) {
  if (const double* d = std::get_if<double>(&literal))
    return std::to_string(*d);
  if (const std::string* s = std::get_if<std::string>(&literal))
    return *s;
  throw std::runtime_error("A Literal should be either a double or a string");
}

std::string TokenTypeToString(TokenType token_type) {
  switch (token_type) {
    case LEFT_PAREN:
      return std::string("LEFT_PAREN");
    case RIGHT_PAREN:
      return std::string("RIGHT_PAREN");
    case LEFT_BRACE:
      return std::string("LEFT_BRACE");
    case RIGHT_BRACE:
      return std::string("RIGHT_BRACE");
    case COMMA:
      return std::string("COMMA");
    case DOT:
      return std::string("DOT");
    case MINUS:
      return std::string("MINUS");
    case PLUS:
      return std::string("PLUS");
    case SEMICOLON:
      return std::string("SEMICOLON");
    case SLASH:
      return std::string("SLASH");
    case STAR:
      return std::string("STAR");
    case BANG:
      return std::string("BANG");
    case BANG_EQUAL:
      return std::string("BANG_EQUAL");
    case EQUAL:
      return std::string("EQUAL");
    case EQUAL_EQUAL:
      return std::string("EQUAL_EQUAL");
    case GREATER:
      return std::string("GREATER");
    case GREATER_EQUAL:
      return std::string("GREATER_EQUAL");
    case LESS:
      return std::string("LESS");
    case LESS_EQUAL:
      return std::string("LESS_EQUAL");
    case IDENTIFIER:
      return std::string("IDENTIFIER");
    case STRING:
      return std::string("STRING");
    case NUMBER:
      return std::string("NUMBER");
    case AND:
      return std::string("AND");
    case CLASS:
      return std::string("CLASS");
    case ELSE:
      return std::string("ELSE");
    case FALSE:
      return std::string("FALSE");
    case FUN:
      return std::string("FUN");
    case FOR:
      return std::string("FOR");
    case IF:
      return std::string("IF");
    case NIL:
      return std::string("NIL");
    case OR:
      return std::string("OR");
    case PRINT:
      return std::string("PRINT");
    case RETURN:
      return std::string("RETURN");
    case SUPER:
      return std::string("SUPER");
    case THIS:
      return std::string("THIS");
    case TRUE:
      return std::string("TRUE");
    case VAR:
      return std::string("VAR");
    case WHILE:
      return std::string("WHILE");
    case END_OF_FILE:
      return std::string("END_OF_FILE");
  }
}

Token::Token(TokenType type, std::string_view lexeme, int line)
    : type_{type},
      lexeme_{lexeme},
      line_{line},
      literal_{std::nullopt} {}

Token::Token(TokenType type, std::string_view lexeme, double literal, int line)
    : type_{type},
      lexeme_{lexeme},
      literal_{Literal{literal}},
      line_{line} {}

Token::Token(TokenType type, std::string_view lexeme,
             std::string literal, int line)
    : type_{type},
      lexeme_{lexeme},
      literal_{Literal{literal}},
      line_{line} {}

TokenType Token::type() const {
  return type_;
}

std::string_view Token::lexeme() const {
  return lexeme_;
}

OptionalLiteral Token::optional_literal() const {
  return literal_;
}

int Token::line() const {
  return line_;
}

std::string Token::ToString() const {
  std::string result{
    TokenTypeToString(type_) + " " + static_cast<std::string>(lexeme_)
  };
  if (!literal_)
    return result;
  if (const double* lit = std::get_if<double>(&literal_.value())) {
    result += " " + std::to_string(*lit);
  } else if (
      const std::string* lit = std::get_if<std::string>(&literal_.value())) {
    result += " " + *lit;
  }
  return result;
}

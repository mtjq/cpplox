#pragma once

#include <string_view>
#include <vector>
#include <unordered_map>

#include "token.h"

class Scanner {
 private:
  static std::unordered_map<std::string, TokenType> keywords_;
  std::string_view source_;
  std::vector<Token> tokens_{std::vector<Token>()};
  int start_{0};
  int current_{0};
  int line_{1};

  void ScanToken();
  void AddToken(TokenType type);
  template<class T>
  void AddToken(TokenType type, T literal);
  bool Match(char expected);
  char Peek();
  char PeekNext();
  char Advance();
  void String();
  void Number();
  void Identifier();
  bool IsAlpha(char c);
  bool IsDigit(char c);
  bool IsAlphaNum(char c);
  bool IsAtEnd();

 public:
  Scanner(std::string_view source);
  std::vector<Token> ScanTokens();
};

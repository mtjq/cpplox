#pragma once

#include <iostream>
#include <string_view>

#include "token.h"

class Runner {
 private:
  static bool has_error_;

  static void Report(int line,
                     std::string_view where,
                     std::string_view message);

 public:
  Runner();
  void Run(std::string_view source);
  void RunFile(char* file_path);
  void RunPrompt();
  static void Error(int line, std::string_view message);
  static void Error(Token token, std::string_view message);
};

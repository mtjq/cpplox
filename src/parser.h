#pragma once

#include <vector>

#include "token.h"
#include "expression.h"

class ParseError : public std::exception {};

class Parser {
 public:
  Parser(std::vector<Token> tokens);
  OptionalExpressionPtr Parse();

 private:
  std::vector<Token> tokens_;
  int current_;

  ExpressionPtr Expression();
  ExpressionPtr Equality();
  ExpressionPtr Comparison();
  ExpressionPtr Term();
  ExpressionPtr Factor();
  ExpressionPtr Unary();
  ExpressionPtr Primary();
  template<typename... TokenType>
  bool Match(TokenType... token_types);
  Token Consume(TokenType token_type, std::string_view message);
  Token Advance();
  Token Previous();
  Token Peek();
  bool Check(TokenType token_type);
  bool IsAtEnd();
  ParseError Error(Token token, std::string_view message);
  void Synchronize();
};

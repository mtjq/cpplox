#pragma once

#include <memory>
#include <variant>
#include <string>

#include "token.h"

// Pre-declaration to create the variant
using Value = std::variant<std::monostate, Literal, bool>;

struct BinaryExpr;
struct GroupingExpr;
struct LiteralExpr;
struct UnaryExpr;

using BinaryExprPtr = std::unique_ptr<BinaryExpr>;
using GroupingExprPtr = std::unique_ptr<GroupingExpr>;
using LiteralExprPtr = std::unique_ptr<LiteralExpr>;
using UnaryExprPtr = std::unique_ptr<UnaryExpr>;

using ExpressionPtr = std::variant<
    BinaryExprPtr, GroupingExprPtr, LiteralExprPtr, UnaryExprPtr>;
using OptionalExpressionPtr = std::optional<ExpressionPtr>;

// Helpers to make pointers to expressions
BinaryExprPtr CreateBinaryExprPtr(
    ExpressionPtr left, Token oper, ExpressionPtr right);
GroupingExprPtr CreateGroupingExprPtr(ExpressionPtr expression);
LiteralExprPtr CreateLiteralExprPtr(); // nil
LiteralExprPtr CreateLiteralExprPtr(Literal lit); // literal
LiteralExprPtr CreateLiteralExprPtr(bool b); // true/false
UnaryExprPtr CreateUnaryExprPtr(Token oper, ExpressionPtr right);

// Expression structs
struct BinaryExpr {
  BinaryExpr(ExpressionPtr left, Token oper, ExpressionPtr right);

  ExpressionPtr left_;
  Token operator_;
  ExpressionPtr right_;
};

struct GroupingExpr {
  GroupingExpr(ExpressionPtr expression);

  ExpressionPtr expression_;
};

struct LiteralExpr {
  LiteralExpr();
  LiteralExpr(Literal lit);
  LiteralExpr(bool b);

  Value value_;
};

struct UnaryExpr {
  UnaryExpr(Token oper, ExpressionPtr right);

  Token operator_;
  ExpressionPtr right_;
};


// Pretty printer
struct AstPrinter {
  virtual std::string operator() (const BinaryExprPtr& expr);
  virtual std::string operator() (const GroupingExprPtr& expr);
  virtual std::string operator() (const LiteralExprPtr& expr);
  virtual std::string operator() (const UnaryExprPtr& expr);
};

std::string Parenthesize(
    std::string_view name, ExpressionPtr& expr);

std::string Parenthesize(
    std::string_view name, ExpressionPtr& expr1, ExpressionPtr& expr2);

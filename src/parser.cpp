#include "parser.h"

#include "runner.h"

Parser::Parser(std::vector<Token> tokens) : tokens_{tokens}, current_{0} {}

OptionalExpressionPtr Parser::Parse() {
  try {
    return Expression();
  } catch (ParseError e) {
    return std::nullopt;
  }
}

ExpressionPtr Parser::Expression() {
  return Equality();
}

ExpressionPtr Parser::Equality() {
  ExpressionPtr expr = Comparison();

  while (Match(BANG_EQUAL, EQUAL_EQUAL)) {
    Token oper = Previous();
    ExpressionPtr right = Comparison();
    expr = CreateBinaryExprPtr(std::move(expr), oper, std::move(right));
  }

  return expr;
}

ExpressionPtr Parser::Comparison() {
  ExpressionPtr expr = Term();

  while (Match(GREATER, GREATER_EQUAL, LESS, LESS_EQUAL)) {
    Token oper = Previous();
    ExpressionPtr right = Term();
    expr = CreateBinaryExprPtr(std::move(expr), oper, std::move(right));
  }
  return expr;
}

ExpressionPtr Parser::Term() {
  ExpressionPtr expr = Factor();

  while (Match(MINUS, PLUS)) {
    Token oper = Previous();
    ExpressionPtr right = Factor();
    expr = CreateBinaryExprPtr(std::move(expr), oper, std::move(right));
  }
  return expr;
}

ExpressionPtr Parser::Factor() {
  ExpressionPtr expr = Unary();

  while (Match(SLASH, STAR)) {
    Token oper = Previous();
    ExpressionPtr right = Unary();
    expr = CreateBinaryExprPtr(std::move(expr), oper, std::move(right));
  }
  return expr;
}

ExpressionPtr Parser::Unary() {
  if (Match(BANG, MINUS)) {
    Token oper = Previous();
    ExpressionPtr right = Unary();
    return CreateUnaryExprPtr(oper, std::move(right));
  }
  return Primary();
}

ExpressionPtr Parser::Primary() {
  if (Match(FALSE))
    return CreateLiteralExprPtr(false);
  if (Match(TRUE))
    return CreateLiteralExprPtr(true);
  if (Match(NIL))
    return CreateLiteralExprPtr();
  if (Match(NUMBER, STRING))
    return CreateLiteralExprPtr(Previous().optional_literal().value());
  if (Match(LEFT_PAREN)) {
    ExpressionPtr expr = Expression();
    Consume(RIGHT_PAREN, "Expect ')' after expression.");
    return CreateGroupingExprPtr(std::move(expr));
  }

  throw Error(Peek(), "Expect expression.");
}

template<typename... TokenType>
bool Parser::Match(TokenType... token_types) {
  for (const auto& token_type: {token_types...}) {
    if (Check(token_type)) {
      Advance();
      return true;
    }
  }
  return false;
}

Token Parser::Consume(TokenType token_type, std::string_view message) {
  if (Check(token_type))
    return Advance();

  throw Error(Peek(), message);
}

Token Parser::Advance() {
  if (!IsAtEnd())
    current_++;
  return Previous();
}

Token Parser::Previous() {
  return tokens_.at(current_ - 1);
}

Token Parser::Peek() {
  return tokens_.at(current_);
}

bool Parser::Check(TokenType token_type) {
  if (IsAtEnd()) {
    return false;
  }
  return Peek().type() == token_type;
}

bool Parser::IsAtEnd() {
  return Peek().type() == END_OF_FILE;
}

ParseError Parser::Error(Token token, std::string_view message) {
  Runner::Error(token, message);
  return ParseError {};
}

void Parser::Synchronize() {
  Advance();
  while (!IsAtEnd()) {
    if (Previous().type() == SEMICOLON)
      return;
    switch (Peek().type()) {
      case CLASS: case FOR: case FUN: case IF: case PRINT: case RETURN:
      case VAR: case WHILE:
        return;
      default:
        break;
    }
    Advance();
  }
}
